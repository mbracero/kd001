## Apuntes

Generación de fichero
~~~~
> mvn archetype:generate
...
(Todos los valores que se introducen son los que vienen por defecto menos los siguientes)
groupId : com.mbracero
artifactId: Kd001
package : com.mbracero
...
~~~~


Para ejecutar la aplicación
~~~~
> mvn compile exec:java -Dexec.mainClass=com.mbracero.App
~~~~
También ejecutar ejecutando fichero run.sh


La base de datos está alojada en mongolab (mongod 2.4.7)
Para conectarnos vía shell:
~~~~
> mongo ds057568.mongolab.com:57568/kd001db -u userkd001 -p Aw3fM7i8Tedf
~~~~

**Esta versión de la aplicación es para ejecutarla en local**

Se añadió configuración para ejecutar la aplicación en heroku (Ver [Getting Started with Java on Heroku](https://devcenter.heroku.com/articles/getting-started-with-java))

Procfile
~~~~
web:    java $JAVA_OPTS -cp target/classes:target/dependency/* com.mbracero.App
~~~~

system.properties
~~~~
java.runtime.version=1.7
~~~~
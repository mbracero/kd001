package com.mbracero.util;

import spark.Request;
import spark.Response;

/**
 * This would keep the Spark interface clean
 * https://github.com/perwendel/spark/issues/44
 */
interface TransformingRoute {
    Object handleAndTransform(Request request, Response response);
}

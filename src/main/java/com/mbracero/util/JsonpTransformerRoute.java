package com.mbracero.util;

import com.google.gson.Gson;

import spark.Request;
import spark.Response;
import spark.ResponseTransformerRoute;

/**
 * Clase de utilidad que transforma las respuestas del servidor al formato correcto para que en cliente obtenga JSONP 
 * @author manuelbracero
 */
public abstract class JsonpTransformerRoute extends ResponseTransformerRoute implements TransformingRoute {
	/**
	 * Gson is a Java library that can be used to convert Java Objects into their JSON representation. It can also be used to convert
	 * a JSON string to an equivalent Java object. Gson can work with arbitrary Java objects including pre-existing objects that you
	 * do not have source-code of. 
	 */
	private Gson gson = new Gson();
	String callback; // Variable para recuperar el callback de cliente
	
	/**
	 * Constructor
	 * @param path
	 */
    protected JsonpTransformerRoute(String path) {
       super(path, "application/json"); // Set contentType to json
    }
    
    /**
     * Invoked when a request is made on this route's corresponding path e.g. '/hello'
     * 
     * @param request The request object providing information about the HTTP request
     * @param response The response object providing functionality for modifying the response
     * 
     * @return The content to be set in the response
     */
    @Override
    public Object handle(Request request, Response response) {
        callback = request.queryParams("callback"); // Get callback from request
        return gson.toJson(handleAndTransform(request, response)); // // Set content type on response to application/json
    }
    
    /**
     * Method called for rendering the output.
     * 
     * @param model
     *            object used to render output.
     * 
     * @return message that it is sent to client.
     */
    @Override
    public String render(Object model) {
	   String jsonResult = gson.toJson(model);
	   String ret = callback + "(" + jsonResult + ")"; // Generate Jsonp
	   return ret;
    }
    
 }

package com.mbracero;

import static spark.Spark.*;

import java.io.IOException;

import com.mbracero.controller.Controller;
import com.mbracero.util.JsonpTransformerRoute;

import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Spark;

public class App  {
	private static final String MONGO_URI = "mongodb://userkd001:Aw3fM7i8Tedf@ds057568.mongolab.com:57568/kd001db"; //"mongodb://localhost";

	private static final String API_KEY = "apikey";
	private static final String USER = "user";
	private static final String PASS = "pass";
	private static final String SESSION_ID = "sessionid";
	
	private static final String LABEL_PORT = "PORT";
	private static final int DEFAULT_PORT = 9001;
	
	private static final String IP_ADDRESS_SPARK = "localhost";
	private static final Integer PORT_SPARK =
			( 
					(System.getenv(LABEL_PORT) != null && (new Integer(System.getenv(LABEL_PORT))) != null )
					?
					Integer.parseInt(System.getenv(LABEL_PORT))
					:
					DEFAULT_PORT
			);

	private static Controller controller;
	
    public static void main( String[] args ) throws IOException {
    	System.out.println("---> main");
    	if (args.length == 0) {
    		new App(MONGO_URI);
    	} else {
    		new App(args[0]);
    	}
    	System.out.println("<--- main");
    }
    
    public App(String mongoURIString) throws IOException {
    	System.out.println(this.getClass().getName() + " :: App IN.");
        setIpAddress(IP_ADDRESS_SPARK);
        setPort(PORT_SPARK);
        
        controller = new Controller(mongoURIString);
        initializeFilters();
        initializeRoutes();
        System.out.println(this.getClass().getName() + " :: App OUT.");
    }
    
    private void initializeFilters() {
    	System.out.println(this.getClass().getName() + " :: initializeFilters IN.");
    	Spark.before(new Filter() {
			@Override
			public void handle(Request request, Response response) {
				System.out.println(this.getClass().getName() + " :: before Filter (*) IN.");
				// si no se esta logeando
				if (!"/login".equals(request.pathInfo())) {
					String apiKeyClient = request.queryParams(API_KEY);
					String sessionid = request.cookie(SESSION_ID);
					
					// Comprobamos la autenticacion del usuario
					boolean authenticated = controller.checkAuthenticatedUser(apiKeyClient, sessionid);
			    	if (!authenticated) {
			    		halt(401, "You are not welcome here");		    		
			    	}
		    	}
				System.out.println(this.getClass().getName() + " :: before Filter (*) OUT.");				
			}
		});
    	System.out.println(this.getClass().getName() + " :: initializeFilters OUT.");
    }
    
    private void initializeRoutes() throws IOException {
    	System.out.println(this.getClass().getName() + " :: initializeRoutes IN.");
        // get
        Spark.get(new JsonpTransformerRoute("/itemsData") {
           @Override
           public Object handleAndTransform(Request request, Response response) {
        	   System.out.println(this.getClass().getName() + " :: get /itemsData IN.");
        	   System.out.println(this.getClass().getName() + " :: get /itemsData OUT.");
        	   return controller.getItemsData();
           }
        });

        // get
        Spark.get(new JsonpTransformerRoute("/itemsModel") {
           @Override
           public Object handleAndTransform(Request request, Response response) {
        	   System.out.println(this.getClass().getName() + " :: get /itemsModel IN.");
        	   System.out.println(this.getClass().getName() + " :: get /itemsModel OUT.");
        	   return controller.getItemsModel();
           }
        });
        
        // get with param
        Spark.get(new JsonpTransformerRoute("/item/:value") {
           @Override
           public Object handleAndTransform(Request request, Response response) {
        	   System.out.println(this.getClass().getName() + " :: get /item/:value IN.");
        	   String value = request.params(":value");
        	   System.out.println(this.getClass().getName() + " :: get /item/:value OUT.");
        	   return controller.findItems(value);
           }
        });
        
        // get with param
        Spark.get(new JsonpTransformerRoute("/tag/:value") {
           @Override
           public Object handleAndTransform(Request request, Response response) {
        	   System.out.println(this.getClass().getName() + " :: get /tag/:value IN.");
        	   String tag = request.params(":value");
        	   System.out.println(this.getClass().getName() + " :: get /tag/:value OUT.");
        	   return controller.findItemsByTag(tag);
           }
        });
        
        // get login
        Spark.get(new JsonpTransformerRoute("/login") {
            @Override
            public Object handleAndTransform(Request request, Response response) {
         	   System.out.println(this.getClass().getName() + " :: get /login IN.");
               String user = request.queryParams(USER);
               String pass = request.queryParams(PASS);
               String apiKeyClient = request.queryParams(API_KEY);
               
               String apikey = controller.authenticateUser(user, pass);
               boolean result = false;
               if (apikey != null && !"".equals(apikey) && apikey.equals(apiKeyClient)) {
            	   result = true;
            	   String sessionid = request.session().id();
            	   response.cookie(SESSION_ID, sessionid);
            	   controller.startSession(sessionid, apikey); // iniciamos la sesion
               }
               
        	   System.out.println(this.getClass().getName() + " :: get /login OUT.");
               return result;
            }
        });
        
        // get logout
        Spark.get(new JsonpTransformerRoute("/logout") {
            @Override
            public Object handleAndTransform(Request request, Response response) {
         	   System.out.println(this.getClass().getName() + " :: get /logout IN.");
         	   String sessionid = request.cookie(SESSION_ID);
         	   
         	   controller.endSession(sessionid);
         	   response.removeCookie(SESSION_ID);
         	   
        	   System.out.println(this.getClass().getName() + " :: get /logout OUT.");
         	   return "";
            }
        });
        
        System.out.println(this.getClass().getName() + " :: initializeRoutes OUT.");
    }
}

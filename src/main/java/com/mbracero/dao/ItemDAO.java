package com.mbracero.dao;

import java.util.List;
import java.util.regex.Pattern;

import com.mbracero.model.Item;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class ItemDAO {
	private static final String COLLECTION = "items";
	
	private final DBCollection listCollection;

	public ItemDAO(final DB database) {
		listCollection = database.getCollection(COLLECTION);
	}
	
	/**
	 * Obtenemos un listado de items de mongodb
	 * @return
	 */
	public List<DBObject> findAllDescendingData(String value) {
	    List<DBObject> list;
	    
	    BasicDBObject query = new BasicDBObject();
	    if (value != null && !"".equals(value)) {
	    	Pattern regex = Pattern.compile(value);
	    	DBObject clause1 = new BasicDBObject("title", regex);
	    	DBObject clause2 = new BasicDBObject("body", regex);
	    	DBObject clause3 = new BasicDBObject("tags", regex);
	    	BasicDBList or = new BasicDBList();
	    	or.add(clause1);
	    	or.add(clause2);
	    	or.add(clause3);
	    	query = new BasicDBObject("$or", or);
	    }
	    DBCursor cursor = listCollection.find(query).sort(new BasicDBObject().append("_id", -1));
	    try {
	    	list = cursor.toArray();
	    } finally {
	        cursor.close();
	    }
	    return list;
	}
	
	/**
	 * Obtenemos un listado de items por tag
	 * @return
	 */
	public List<DBObject> findAllByTagDescendingData(String tag) {
	    List<DBObject> list;
	    
	    BasicDBObject query = new BasicDBObject();
	    if (tag != null && !"".equals(tag)) {
	    	Pattern regex = Pattern.compile(tag);
	    	query = new BasicDBObject("tags", regex);
	    }
	    DBCursor cursor = listCollection.find(query).sort(new BasicDBObject().append("_id", -1));
	    try {
	    	list = cursor.toArray();
	    } finally {
	        cursor.close();
	    }
	    return list;
	}
		
	/**
	 * Obtenemos un listado de items de mongodb y lo convertimos a Objetos de modelo
	 * @return
	 */
	public List<Item> findAllDescendingModel() {
		List<Item> list = Item.convertListDBObjectToListItem(findAllDescendingData(""));
		
	    return list;
	}
	
	/**
	 * Buscamos items por valor que lo contenga en todos sus atributos
	 * @param value
	 * @return
	 */
	public List<Item> findItems(String value) {
		List<Item> list = Item.convertListDBObjectToListItem(findAllDescendingData(value));
		
	    return list;
	}
	/**
	 * Buscamos items por valor de tag
	 * @param tag
	 * @return
	 */
	public List<Item> findItemsByTag(String tag) {
		List<Item> list = Item.convertListDBObjectToListItem(findAllByTagDescendingData(tag));
		
	    return list;
	}
}

package com.mbracero.dao;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class UserDAO {
    private final DBCollection usersCollection;

    public UserDAO(DB database) {
        usersCollection = database.getCollection("users");
    }
    
    public String validateLogin(String username, String password) {
        DBObject user;

        user = usersCollection.findOne(new BasicDBObject("_id", username));

        if (user == null) {
            System.out.println("User not in database");
            return null;
        }

        String pass = user.get("password").toString();
        
        if (!pass.equals(password)) {
            System.out.println("Submitted password is not a match");
            return null;
        }

        return user.get("apikey").toString();
    }
}


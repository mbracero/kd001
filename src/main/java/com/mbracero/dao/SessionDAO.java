package com.mbracero.dao;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class SessionDAO {
    private final DBCollection sessionsCollection;

    public SessionDAO(DB blogDatabase) {
        sessionsCollection = blogDatabase.getCollection("sessions");
    }
    
    // starts a new session in the sessions table
    public String startSession(String sessionid, String apikey) {
        // build the BSON object
        BasicDBObject session = new BasicDBObject("key", apikey);

        session.append("_id", sessionid);

        sessionsCollection.insert(session);

        return session.getString("_id");
    }

    // ends the session by deleting it from the sessions table
    public void endSession(String sessionID) {
        sessionsCollection.remove(new BasicDBObject("_id", sessionID));
    }

    // retrieves the session from the sessions table
    public String getSession(String sessionID) {
    	DBObject session = sessionsCollection.findOne(new BasicDBObject("_id", sessionID));
        return ( session != null ? session.get("key").toString() : "");
    }
}


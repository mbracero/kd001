package com.mbracero.controller;

import java.io.IOException;
import java.util.List;

import com.mbracero.dao.ItemDAO;
import com.mbracero.dao.SessionDAO;
import com.mbracero.dao.UserDAO;
import com.mbracero.model.Item;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class Controller {
	private static final String DB = "kd001db";
	
	private final ItemDAO itemDAO;
	private final UserDAO userDAO;
	private final SessionDAO sessionDAO;
	
	public Controller(String mongoURIString) throws IOException {
        final MongoClient mongoClient = new MongoClient(new MongoClientURI(mongoURIString));
        final DB database = mongoClient.getDB(DB);

        itemDAO = new ItemDAO(database);
        userDAO = new UserDAO(database);
        sessionDAO = new SessionDAO(database);
    }
	
	public List<DBObject> getItemsData() {
	  return itemDAO.findAllDescendingData("");
	}
	
	public List<Item> getItemsModel() {
	  return itemDAO.findAllDescendingModel();
	}
	
	public List<Item> findItems(String value) {
	  return itemDAO.findItems(value);
	}
	
	public List<Item> findItemsByTag(String tag) {
	  return itemDAO.findItemsByTag(tag);
	}
	
	public String authenticateUser(String user, String pass) {
		return userDAO.validateLogin(user, pass);
	}

	public boolean checkAuthenticatedUser(String apiKeyClient, String sessionid) {
		String userkey = sessionDAO.getSession(sessionid);
		return userkey.equals(apiKeyClient);
	}

	public String startSession(String sessionid, String apikey) {
		return sessionDAO.startSession(sessionid, apikey);
	}

	public void endSession(String sessionid) {
		sessionDAO.endSession(sessionid);
	}	
}

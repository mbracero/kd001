package com.mbracero.model;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class Item {
    private String id;
    private String title;
    private String body;
    private List<String> tags;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getTitle() {
		return title;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	/**
	 * Metodos de utilidad
	 */
	@SuppressWarnings("unchecked")
	public static Item convertDBObjectToItem(DBObject dbObject) {
		if (dbObject == null) {
			return null;
		}
		Item item = new Item();
		if (dbObject.get("_id") != null && !"".equals(dbObject.get("_id").toString())) {
			item.setId(dbObject.get("_id").toString());
		}
		item.setTitle(dbObject.get("title").toString());
		item.setBody(dbObject.get("body").toString());
		item.setTags((List<String>) dbObject.get("tags"));
		return item;
	}
	public static List<Item> convertListDBObjectToListItem(List<DBObject> listDBObject) {
		if (listDBObject == null || listDBObject.size() <= 0) {
			return null;
		}
		List<Item> listItems = new ArrayList<Item>();
		for (DBObject dbObject : listDBObject) {
			listItems.add(convertDBObjectToItem(dbObject));
		}
		return listItems;
	}

	public static DBObject convertItemToDBObject(Item item) {
		if (item == null) {
			return null;
		}
		BasicDBObject dbObject = new BasicDBObject();
		if (item.getId() != null && !"".equals(item.getId())) {
			dbObject.append("_id", item.getId());
		}
		dbObject.append("title", item.getTitle());
		dbObject.append("body", item.getBody());
		dbObject.append("tags", item.getTags());
		return dbObject;
	}
	public static List<DBObject> convertListItemToListDBObject(List<Item> listItem) {
		if (listItem == null || listItem.size() <= 0) {
			return null;
		}
		List<DBObject> listDBObject = new ArrayList<DBObject>();
		for (Item item : listItem) {
			listDBObject.add(convertItemToDBObject(item));
		}
		return listDBObject;
	}
	
}
